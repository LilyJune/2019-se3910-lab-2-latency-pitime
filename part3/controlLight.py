# course: SE3910 Real Time SYstems
# laboratory: 2
# date: July, 2018
# username: wschilling
# name: Walter Schilling
# description: This is a basic python program that demonstrates latency by controlling a light based upon an input.

import sys
from time import sleep

# Start the program
print("This program will control the led light.")
print ("usage: controlLight <light gpio pin> <pushbutton gpio pin> <delay ms> ")

# Set up the  file system.
exportFile = open("/sys/class/gpio/export", "w", 0)
exportFile.write(sys.argv[1])
exportFile.close()

# Set up the  file system.
exportFile = open("/sys/class/gpio/export", "w", 0)
exportFile.write(sys.argv[2])
exportFile.close()

# Set the direction to be output.
directionString = "/sys/class/gpio/gpio"+sys.argv[1]+"/direction"
print(directionString)
directionFile = open(directionString, "w", 0) 
directionFile.write("out")
directionFile.close()

directionString = "/sys/class/gpio/gpio"+sys.argv[2]+"/direction"
print(directionString)
directionFile = open(directionString, "w", 0) 
directionFile.write("in")
directionFile.close()

# Open the Value file to write out the outputs.
valueString = "/sys/class/gpio/gpio"+sys.argv[1]+"/value"

# Open the value file to read the state of the input pushbutton
pushbuttonValueString = "/sys/class/gpio/gpio"+sys.argv[2]+"/value"

# Now write the  leds values out.
keepGoing = 1

try:
	while (keepGoing == 1):
		# read the pushbuton status
		pushButtonFile = open(pushbuttonValueString, "r", 0)
		state = pushButtonFile.readline()
		pushButtonFile.close();
#		print (state)

		buttonValue = int(state)

		if (buttonValue>0):
			valueFile = open(valueString, "w", 0)
			valueFile.write("0")
			valueFile.close();
		else :
			valueFile = open(valueString, "w", 0)
			valueFile.write("1")
			valueFile.close();
                if (sys.argv[3] > 0):
                        sleep(float(sys.argv[3])/1000)


except KeyboardInterrupt:
	print("W: interrupt received, stopping...")
finally:

# Now exit the program.
	exportFile = open("/sys/class/gpio/unexport", "w", 0)
	exportFile.write(sys.argv[1])
	exportFile.write(sys.argv[2])
	exportFile.close()

