# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/se3910/2019-se3910-lab-2-latency-pitime/part2/src/GPIO.cpp" "/home/se3910/2019-se3910-lab-2-latency-pitime/part2/build/CMakeFiles/lab2part2.dir/GPIO.cpp.o"
  "/home/se3910/2019-se3910-lab-2-latency-pitime/part2/src/Lab2Part2.cpp" "/home/se3910/2019-se3910-lab-2-latency-pitime/part2/build/CMakeFiles/lab2part2.dir/Lab2Part2.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/rpi_sysroot/usr/local/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
