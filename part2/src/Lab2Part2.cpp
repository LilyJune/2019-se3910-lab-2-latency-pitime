/**
 * @file
 * @author  Walter Schilling (schilling@msoe.edu)
 * @version 2.0
 *
 * @section LICENSE This code is distributed to students in order that they complete lab 2 on latency and performance.
 *
 *
 * @section DESCRIPTION
 *
 * This program will blink a light at a given rate.  Because of it's usage of the real time extensions of the OS, it must be run as the super user.
 */

#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include "GPIO.h"
#include <unistd.h>


using namespace se3910RPi;
using namespace std;

/**
 * This program will blink a light on a given GPIO on and off.
 * The rate and number of blinks are set by the user.
 */
int main(int argc, char* argv[])
{
	// Check to determine if the command line usage is correct or not.
	if (argc != 4)
	{
		cerr << "Usage: " << argv[0] << " <GPIO Pin Number> <Blink Rate in Blinks per second> <Number Of Blinks>";
		exit(-1);
	}

	// Determine the period and the blink count.
	int GPIOPin = atoi(argv[1]);
	int period = 500000 / atoi(argv[2]); // The period is 500000 us / the number of blinks per second.
	int blinkCount = atoi(argv[3]);

	// Instantiate a new instance of a GPIO port.
	GPIO outGPIO(GPIOPin, GPIO::GPIO_OUT);

	// Setup the operating thread to be a real time thread.
	struct sched_param p;
	p.__sched_priority = sched_get_priority_max(SCHED_FIFO);

	if (sched_setscheduler(0, SCHED_FIFO, &p) != 0) {
		printf("Failed to set the scheduler\n");
		exit(-1);
	}

	// Loop over the data, turning things on and off as is necessary.
	for (int index = 0; index < blinkCount; index++)
	{
		// Turn the light on.
		outGPIO.setValue(GPIO::GPIO_HIGH);

		// Cause the thread to sleep for a given period of time.
		usleep(period);

		// Turn the pin off.
		outGPIO.setValue(GPIO::GPIO_LOW);

		// Go to sleep for a given period of time.
		usleep(period);
	}
}
