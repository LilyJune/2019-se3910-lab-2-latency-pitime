# course: SE3910 Real Time SYstems
# laboratory: 2
# date: July, 2018
# username: wschilling
# name: Walter Schilling
# description: This is a basic python program that demonstrates how to blink a light in python on the Raspberry PI.
import sys
from time import sleep

# Start the program
print("This program will blink the led light.")

# Set up the  file system.
exportFile = open("/sys/class/gpio/export", "w", 0)
# Argv1 represents the GPIO pin that is to be blinked.
exportFile.write(sys.argv[1])
exportFile.close()

# Set the direction to be output so that the program can control the pin.
directionString = "/sys/class/gpio/gpio"+sys.argv[1]+"/direction"
directionFile = open(directionString, "w", 0) 
directionFile.write("out")
directionFile.close()

# Determine the file that is to be written to in order to control the given pin.
valueString = "/sys/class/gpio/gpio"+sys.argv[1]+"/value"

# Determine based upon argv[2] how many times per second the light is to blink.  The sleep time will be the period of the periodic signal.
sleepTime = .5 / float(sys.argv[2])
try:
	for x in range(0, int(sys.argv[3])):
		# Open the IO file.
		valueFile = open(valueString, "w", 0)
		valueFile.write("1")
		# The close will write the value to the file so that it takes effect.
		valueFile.close()
		# Delay until the next change is to occur.
		sleep(sleepTime)
		valueFile = open(valueString, "w", 0)
		valueFile.write("0")
		sleep(sleepTime)
		# The close will write the value to the file.
		valueFile.close()
		
except KeyboardInterrupt:
	print("W: interrupt received, stopping...")
	
finally:
	# Now exit the program, cleaning up the GPIO as we go.
	exportFile = open("/sys/class/gpio/unexport", "w", 0)
	exportFile.write(sys.argv[1])
	exportFile.close()





