/**
 * @file
 * @author  Walter Schilling (schilling@msoe.edu)
 * @version 1.0
 *
 * @section LICENSE This code is distributed to students in order that they complete lab 2 on latency and performance.
 *
 *
 * @section DESCRIPTION
 *
 * This program will allow the user to control a light.  The light will be controlled by pressing and releasing a pushbutton.
 * In this code, the pushbutton is read via an interrupt.
 */
#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include "GPIO.h"

using namespace se3910RPi;
using namespace std;

/**
 * This program will control the LED.  It essentially will turn the LED on if the button is pressed and off if the button is released.
 * However, this version uses interrupts instead of polling.
 */
int main(int argc, char* argv[]) {
	// Check to determine if the command line usage is correct or not.
	if (argc != 3) {
		cerr << "Usage: " << argv[0]
				<< " <Output GPIO Pin Number> <Input GPIO Pin Number>";
		exit(-1);
	}

	// Determine the period and the blink count.
	int GPIOPout = atoi(argv[1]);
	int GPIOPin = atoi(argv[2]);

	// Instantiate a new instance of a GPIO port.
	GPIO outGPIO(GPIOPout, GPIO::GPIO_OUT);
	GPIO inGPIO(GPIOPin, GPIO::GPIO_IN);

	// Setup the operating thread to be a real time thread.
	struct sched_param p;
	p.__sched_priority = sched_get_priority_max(SCHED_FIFO);

	if (sched_setscheduler(0, SCHED_FIFO, &p) != 0) {
		printf("Failed to set the scheduler\n");
		exit(-1);
	}

	// Setup the device to trigger edge interrupts on both rising and falling.
	inGPIO.enableEdgeInterrupt(GPIO::GPIO_BOTH);

	// Loop over the data, turning things on and off as is necessary.
	while (1 == 1) {
		if (inGPIO.waitForEdge(10000) == 0) {
			// We waited and received an edge triggered interrupt, not a timeout.
			// Read the current state of the pin and apply it.
			if (inGPIO.getValue() == GPIO::GPIO_LOW) {
				// Turn the light on.
				outGPIO.setValue(GPIO::GPIO_HIGH);
			} else {
				// Turn the pin off.
				outGPIO.setValue(GPIO::GPIO_LOW);
			}
		}
	}
}
