/**
 * @file
 * @author  Walter Schilling (schilling@msoe.edu)
 * @version 2.0
 *
 * @section LICENSE This code is distributed to students in order that they complete lab 2 on latency and performance.
 *
 *
 * @section DESCRIPTION
 *
 * This program will allow the user to control a light.  The light will be controlled by pressing and releasing a pushbutton.
 */

#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include "GPIO.h"
#include <unistd.h>

using namespace se3910RPi;
using namespace std;

/**
 * This program will control the LED.  It essentially will turn the LED on if the button is pressed and off if the button is released.
 */
int main(int argc, char* argv[]) {
	// Check to determine if the command line usage is correct or not.
	if (argc != 4) {
		cerr << "Usage: " << argv[0]
				<< " <Output GPIO Pin Number> <Input GPIO Pin Number> <Delay in ms as an integer>";
		exit(-1);
	}

	// Determine the period and the blink count.
	int GPIOPout = atoi(argv[1]);
	int GPIOPin = atoi(argv[2]);
	int sleepLength = atoi(argv[3]) * 1000;

	// Instantiate a new instance of a GPIO port.
	GPIO outGPIO(GPIOPout, GPIO::GPIO_OUT);
	GPIO inGPIO(GPIOPin, GPIO::GPIO_IN);

	// Setup the operating thread to be a real time thread.
	struct sched_param p;
	p.__sched_priority = sched_get_priority_max(SCHED_FIFO);

	printf("%d\n", p.__sched_priority);

	if (sched_setscheduler(0, SCHED_FIFO, &p) != 0) {
		printf("Failed to set the scheduler\n");
		exit(-1);
	}

	// Loop over the data, turning things on and off as is necessary.
	while (1 == 1) {
		if (inGPIO.getValue() == GPIO::GPIO_LOW) {

			// Turn the light on.
			outGPIO.setValue(GPIO::GPIO_HIGH);
		} else {
			// Turn the pin off.
			outGPIO.setValue(GPIO::GPIO_LOW);
		}
		if (sleepLength != 0) {
			usleep(sleepLength);
		}
	}
}
